import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:get_version/get_version.dart';

void main() => runApp(MyApp());

Future<Map<String, String>> getInfo() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  return {
    'androidVersion': (await deviceInfo.androidInfo).version.release,
    'model': (await deviceInfo.androidInfo).model,
    'display': (await deviceInfo.androidInfo).display,
    'projectVersion': (await GetVersion.projectVersion),
    'appID': (await GetVersion.appID),
    'appName': (await GetVersion.appName),
    'platformVersion': (await GetVersion.platformVersion),
    'projectCode': (await GetVersion.projectCode),
  };
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Device info and get version'),
        ),
        body: FutureBuilder<Map<String, String>>(
          future: getInfo(),
          builder: (ctx, snapshot) {
            return snapshot.connectionState == ConnectionState.done
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Column(
                        children: snapshot.data.entries
                            .map((entry) => Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(entry.key),
                                    Text(entry.value),
                                  ],
                                ))
                            .toList(),
                      ),
                    ),
                  )
                : Center();
          },
        ),
      ),
    );
  }
}
